#include "tset.h"

TSet::TSet(const int mp) : BitField(mp) {
  max_cardinality_ = mp;
}

TSet::TSet(const TSet& s) : BitField(s.bit_field_){
  max_cardinality_ = s.max_cardinality_;
}

TSet::TSet(const TBitField& bf) : BitField(bf) {
  max_cardinality_ = bf.GetLength();
}

TSet::operator TBitField() {
  return bit_field_;
}

int TSet::GetMaxPower() const {
  return max_cardinality_;
}

int TSet::IsMember(const int elem) const {
  return bit_field_.GetBit(Elem);
}

void TSet::InsElem(const int elem) {
  bit_field_.SetBit(Elem);
}

void TSet::DelElem(const int elem) {
  bit_field_.ClearBit(Elem);
}

TSet& TSet::operator=(const TSet& s){
  max_cardinality_ = s.max_cardinality_;
  bit_field_ = s.bit_field_;

  return *this;
}

bool TSet::operator==(const TSet& s) const {
  if (bit_field_ == s.bit_field_)
    return true;

  return false;
}

bool TSet::operator!=(const TSet& s) const {
  return !(*this == s);
}

TSet TSet::operator+(const TSet& s) {
  TSet temp = bit_field_ | s.bit_field_;

  return temp;
}

TSet TSet::operator+(const int elem) {
  TSet temp = *this;
  temp.InsElem(Elem);

  return temp;
}

TSet TSet::operator-(const int elem){
  TSet temp = *this;
  temp.DelElem(Elem);

  return temp;
}

TSet TSet::operator*(const TSet& s) {
  TSet temp = bit_field_ & s.bit_field_;

  return temp;
}

TSet TSet::operator~() {
  TSet temp = ~bit_field_;

  return temp;
}

std::istream& operator>>(std::istream& istr, TSet& s) {
  char delimeter;
  int curr_elem;

  while (true) {
    istr >> curr_elem;
    s.InsElem(curr_elem);

    istr >> delimeter;
    if (delimeter != ',')
      break;
  }

  return istr;
}

std::ostream& operator<<(std::ostream& ostr, const TSet& s) {
  bool is_this_first = true;

  ostr << "{";

  for (int i = 0; i < s.max_cardinality_; i++) {
    if (s.IsMember(i)) {
      if (!is_this_first)
        ostr << ", ";
      else
        is_this_first = false;

      ostr << "i";
    }
  }

  ostr << "}";

  return ostr;
}
