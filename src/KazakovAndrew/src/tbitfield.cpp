#include "tbitfield.h"

#include <iostream>

TBitField::TBitField(const int len) : length_(len) {
  if (len < 0) {
    throw 1;
  }

  bit_field_length_ = len / kBitsInElement + (bool)(len % kBitsInElement);
  bit_field_ = new unsigned int[bit_field_length_];

  for (int i = 0; i < bit_field_length_; i++)
    bit_field_[i] = 0;
}

TBitField::TBitField(const TBitField& bf) {
  length_ = bf.length_;
  bit_field_length_ = bf.bit_field_length_;

  bit_field_ = new unsigned int[bit_field_length_];

  for (int i = 0; i < bit_field_length_; i++) {
    bit_field_[i] = bf.bit_field_[i];
  }
}

TBitField::~TBitField() {
  delete[] bit_field_;
}

int TBitField::GetMemIndex(const int bit) const {
  if (bit < 0 || bit >= length_) {
    throw 3;
  }

  return bit / kBitsInElement;
}

unsigned int TBitField::GetMemMask(const int bit) const {
  if (bit < 0 || bit >= length_) {
    throw 3;
  }

  return 1 << (bit - GetMemIndex(bit)*kBitsInElement);
}

int TBitField::GetLength() const {
  return length_;
}

void TBitField::SetBit(const int bit){
  if (bit < 0 || bit >= length_) {
    throw 3;
  }

  bit_field_[GetMemIndex(bit)] |= GetMemMask(bit);
}

void TBitField::ClearBit(const int bit) {
  if (bit < 0 || bit >= length_) {
    throw 3;
  }

  bit_field_[GetMemIndex(bit)] &= ~GetMemMask(bit);
}

int TBitField::GetBit(const int bit) const {
  if (bit < 0 || bit >= length_) {
    throw 3;
  }

  return (bool)(bit_field_[GetMemIndex(bit)] & GetMemMask(bit));
}

TBitField& TBitField::operator=(const TBitField& bf) {
  if (&bf != this) {
    delete[] bit_field_;

    length_ = bf.length_;
    bit_field_length_ = bf.bit_field_length_;

    bit_field_ = new unsigned int[bit_field_length_];

    for (int i = 0; i < bit_field_length_; i++)
      bit_field_[i] = bf.bit_field_[i];
  }

  return *this;
}

bool TBitField::operator==(const TBitField& bf) const {
  if (length_ != bf.length_)
    return false;

  for (int i = 0; i < length_; i++)
    if (GetBit(i) != bf.GetBit(i))
      return false;

  return true;
}

bool TBitField::operator!=(const TBitField& bf) const {
  return !(*this == bf);
}

TBitField TBitField::operator|(const TBitField& bf) {
  TBitField temp((bf.length_ > length_) ? bf.length_ : length_);

  for (int i = 0; i < bit_field_length_; i++)
    temp.bit_field_[i] = bit_field_[i] | bf.bit_field_[i];

  return temp;
}

TBitField TBitField::operator&(const TBitField& bf) {
  TBitField temp((bf.length_ > length_) ? bf.length_ : length_);

  for (int i = 0; i < bit_field_length_; i++)
    temp.bit_field_[i] = bit_field_[i] & bf.bit_field_[i];

  return temp;
}

TBitField TBitField::operator~() {
  TBitField temp(length_);

  for (int i = 0; i < length_; i++) {
    if (GetBit(i)) {
      temp.ClearBit(i);
    } else {
      temp.SetBit(i);
    }
  }

  return temp;
}

std::istream& operator>>(std::istream& istr, TBitField& bf) {
  char curr_bit_value;

  for (int i = 0; i < bf.length_; i++) {
    istr >> curr_bit_value;

    if (curr_bit_value == 0) {
      bf.ClearBit(i);
    } else if (curr_bit_value == 1) {
      bf.SetBit(i);
    } else {
      break;
    }
  }

  return istr;
}

std::ostream& operator<<(std::ostream& ostr, const TBitField& bf) {
  for (int i = 0; i < bf.length_; i++)
    ostr << bf.GetBit(i);

  return ostr;
}
