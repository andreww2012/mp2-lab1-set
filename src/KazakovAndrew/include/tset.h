#ifndef __SET_H__
#define __SET_H__

#include "tbitfield.h"

class TSet {
public:
  explicit TSet(const int);
  TSet(const TSet&);
  TSet(const TBitField&);
  TSet& operator=(const TSet&);
  operator TBitField(void);  // преобразование типа к битовому полю

  int GetMaxCardinality(void) const;
  void InsElem(const int elem);
  void DelElem(const int elem);
  bool IsMember(const int elem) const;

  bool operator==(const TSet&) const;
  bool operator!=(const TSet&) const;
  TSet operator+(const int elem);
  TSet operator-(const int elem);
  TSet operator+(const TSet&);
  TSet operator*(const TSet&);
  TSet operator~(void);

private:
  int max_cardinality_;
  TBitField bit_field_;

  friend std::istream& operator>>(std::istream& istr, TSet& bf);
  friend std::ostream& operator<<(std::ostream& ostr, const TSet& bf);
};

#endif
