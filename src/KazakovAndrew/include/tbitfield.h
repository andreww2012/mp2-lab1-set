#ifndef __BITFIELD_H__
#define __BITFIELD_H__

#include <iostream>

// Список исключений:
// 1 - отрицательная длина битового поля
// 2 - не удалось выделить память для создания битового поля
// 3 - выход за пределы битового поля (бит с указанным номером не существует)
class TBitField {
public:
  static const int kBitsInByte = 8;
  static const int kBitsInElement = sizeof(unsigned int) * 8;

  explicit TBitField(const int length);
  TBitField(const TBitField& bf);
  TBitField& operator=(const TBitField& bf);
  ~TBitField(void);

  int GetLength(void) const;
  int GetBit(const int bit) const;

  void SetBit(const int bit);
  void ClearBit(const int bit);

  bool operator==(const TBitField&) const;
  bool operator!=(const TBitField&) const;
  TBitField operator|(const TBitField&);
  TBitField operator&(const TBitField&);
  TBitField operator~(void);

private:
  int length_;  // макс. кол-во битов

  unsigned int* bit_field_;
  int bit_field_length_;

  int GetMemIndex(const int bit) const;
  unsigned int GetMemMask(const int bit) const;

  friend std::istream& operator>>(std::istream& istr, TBitField& bf);
  friend std::ostream& operator<<(std::ostream& ostr, const TBitField& bf);
};

#endif
